/**
 * Sample transaction
 * @param {org.example.ItemTransfer} itemTransfer
 * @transaction
 */

function itemTransfer(itemTransfer) {
    if(itemTransfer.from.stock < itemTransfer.amount) {
        throw new Error('Item stock is not enough!');
    }
      if (itemTransfer.from.code != itemTransfer.code) {
        throw new Error('Item Code not matched!');
      }

    itemTransfer.from.stock -= itemTransfer.amount;
    itemTransfer.to.stock += itemTransfer.amount;

    return getAssetRegistry('org.example.Item').then(function(assetRegistry) {
        return assetRegistry.updateAll([itemTransfer.from,itemTransfer.to]);
    });
}

/**
 * To withDraw amount from an item
 * @param {org.example.withDraw} withDraw 
 * @transaction
 */
function withdraw(withDraw) {

    withDraw.itemId.stock -= withDraw.amount;

    return getAssetRegistry('org,example.Item').then(function(assetRegistry) {
        return assetRegistry.update(withDraw.Id);
    });
}