/**
* @param {org.handson.PointUpdate} pointupdate
* @transaction
*/

function pointupdate(pointupdate) {
    
    pointupdate.from.Date = pointupdate.Date;
    pointupdate.from.point += pointupdate.point;
        
        return getAssetRegistry('org.handson.yourCourse')
        .then (function (assetRegistry) {
        return assetRegistry.update(pointupdate.from);
        })
        .then (function () {
        return getAssetRegistry('org.handson.yourCourse');
        });
        
        }